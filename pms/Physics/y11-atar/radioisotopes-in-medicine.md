# Part of the Nuclear Physics Investigation

Nuclear Medicine 101
	• Nuclear medicine uses radiation to provide diagnostic information 
		○ About the functioning of a person's organs/body
	• Radiotherapy is often used to treat medical conditions like cancer
		○ The radiation is used to damage, weaken, or kill specific targeted cells
	• The demand for radioisotopes for use in medicine increases by 5% each year
		○ Over 40 million procedures are conducted each year
	• Sterilization of medical equipment - very important



References
	• Reference 1 - http://www.world-nuclear.org/information-library/non-power-nuclear-applications/radioisotopes-research/radioisotopes-in-medicine.aspx - World Nuclear Association
